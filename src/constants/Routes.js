export const MAIN = '/';
export const LOGIN = '/login';
export const LOGOUT = '/logout';
export const DOCUMENTS = '/documents';
export const DOCUMENT = '/document';
