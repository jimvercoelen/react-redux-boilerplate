import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

const reducers = {

};

export default combineReducers({ ...reducers, routing });
