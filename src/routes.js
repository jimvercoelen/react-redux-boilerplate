import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import * as Routes from './constants/Routes';
import Layout from './containers/Layout/Layout';
import Login from './containers/Login/Login';

export default function createRoutes () {
  return (
    <Route path={Routes.MAIN} component={Layout}>
      <Route path={Routes.LOGIN} component={Login} />
      <IndexRedirect to={Routes.LOGIN} />
    </Route>
  );
};
