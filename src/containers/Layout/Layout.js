import React, { PropTypes } from 'react';
import View from '../../components/View/View';
import Header from '../../components/Header/Header';
import 'sanitize.css/sanitize.css';
import './Layout.scss';

const Layout = ({ children }) => {
  return (
    <div>
      <Header />
      <View>
        {children}
      </View>
    </div>
  );
};

Layout.propTypes = {
  // Empty
};

export default Layout;
