import React, { PropTypes } from 'react';
import FormField from './FormField/FormField';

const Form = ({ children, ...props }) => (
  <form {...props} noValidate>
    {children}
  </form>
);

Form.propTypes = {
  children: PropTypes.node.isRequired
};

Form.Field = FormField;

export default Form;
