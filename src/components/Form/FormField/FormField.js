import React, { PropTypes } from 'react';
import styles from './FormField.scss';

const FormField = ({ children }) => (
  <div className={styles.component}>
    {children}
  </div>
);

FormField.propTypes = {
  children: PropTypes.node.isRequired
};

export default FormField;
