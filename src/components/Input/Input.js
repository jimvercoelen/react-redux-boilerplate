import React, { PropTypes } from 'react';
import cx from 'classnames';
import styles from './Input.scss';

const Input = ({ error, ...props }) => (
  <input
    className={cx({
      [styles.component]: true,
      [styles.error]: error
    })}
    {...props} />
);

Input.propTypes = {
  error: PropTypes.any
};

export default Input;
