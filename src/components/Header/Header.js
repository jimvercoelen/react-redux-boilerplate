import React, { PropTypes } from 'react';
import styles from './Header.scss';

const Header = ({ }) => {
  return (
    <div className={styles.component}>
      <div className={styles.content}>
        <div className={styles.controls} />
      </div>
    </div>
  );
};

Header.propTypes = {
  // Empty
};

export default Header;
