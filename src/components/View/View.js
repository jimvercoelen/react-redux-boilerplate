import React, { PropTypes } from 'react';
import styles from './View.scss';

const View = ({ children }) => (
  <div className={styles.component}>
    {children}
  </div>
);

View.propTypes = {
};

export default View;
