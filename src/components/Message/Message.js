import React, { PropTypes } from 'react';
import cx from 'classnames';
import styles from './Message.scss';

const Message = ({ error, children }) => (
  <div className={cx({
    [styles.component]: true,
    [styles.error]: error
  })}>
    {children}
  </div>
);

Message.propTypes = {
  error: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default Message;
