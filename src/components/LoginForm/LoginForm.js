import React, { Component, PropTypes } from 'react';
import styles from './LoginForm.scss';
import _ from 'lodash';

import Form from '../Form/Form';
import Input from '../Input/Input';
import Button from '../Button/Button';
import Message from '../Message/Message';

export default class LoginForm extends Component {
  state = {
    fields: {
      email: '',
      password: ''
    },
    errors: {}
  };

  static propTypes = {
    onSubmit: PropTypes.func.isRequired
  };

  handleUpdateField = fieldName => (event) => {
    this.setState({
      fields: {
        ...this.state.fields,
        [fieldName]: event.target.value
      }
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    this.validate();

    if (_.isEmpty(this.state.errors)) {
      this.props.onSubmit(this.state.fields);
    }
  };

  validate = () => {
    let errors = {};
    const { fields: { email, password } } = this.state;

    if (password === '') {
      errors.password = 'A password is required';
    }

    if (email === '') {
      errors.email = 'An email is required';
    }

    this.setState({ errors });
  };

  render () {
    const { fields: { email, password }, errors } = this.state;

    return (
      <div className={styles.component}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Field>
            <label>
              E-mail
            </label>
            <Input
              error={errors.email}
              value={email}
              type="email"
              placeholder="E-mail"
              onChange={this.handleUpdateField('email')} />
            {errors.email ? (
              <Message error>
                {errors.email}
              </Message>
            ) : null}
          </Form.Field>
          <Form.Field>
            <label>
              Password
            </label>
            <Input
              error={errors.password}
              value={password}
              type="password"
              placeholder="Password"
              onChange={this.handleUpdateField('password')} />
            {errors.password ? (
              <Message error>
                {errors.password}
              </Message>
            ) : null}
          </Form.Field>
          <Button primary>
            Sign In
          </Button>
        </Form>
      </div>
    );
  }
}
