import React, { PropTypes } from 'react';
import cx from 'classnames';
import styles from './Button.scss';

const Button = ({ children, primary, ...props }) => (
  <button
    className={cx({
      [styles.component]: true,
      [styles.primary]: primary
    })}
    {...props}>
    <div className={styles.content}>
      {children}
    </div>
  </button>
);

Button.propTypes = {
  children: PropTypes.node.isRequired,
  primary: PropTypes.bool
};

export default Button;
