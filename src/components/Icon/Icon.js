import React, { PropTypes } from 'react';
import cx from 'classnames';
import styles from './Icon.scss';

const Icon = ({ glyph, className }) => (
  <svg
    className={cx({
      [styles.component]: true,
      [className]: className
    })}>
    <use xlinkHref={glyph} />
  </svg>
);

Icon.propTypes = {
  glyph: PropTypes.string.isRequired,
  className: PropTypes.string
};

export default Icon;
