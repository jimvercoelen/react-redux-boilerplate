const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const LodashPlugin = require('lodash-webpack-plugin');

const paths = require('./paths');
const env = require('./env');

const entryPath = path.resolve(paths.srcPath, 'index.js');
const indexHtmlPath = path.resolve(paths.srcPath, 'index.html');

const entry = env.isDev ? [
    'react-hot-loader/patch',
    entryPath
  ] : entryPath;

const globals = {
  'process.env': {
    'NODE_ENV': JSON.stringify(env.env)
  }
};

let htmlWebpackPluginConfig = {
  template: indexHtmlPath,
  inject: true
};

if (env.isProd) {
  htmlWebpackPluginConfig.minify = {
    removeComments: true,
    collapseWhitespace: true,
    removeRedundantAttributes: true,
    useShortDoctype: true,
    removeEmptyAttributes: true,
    removeStyleLinkTypeAttributes: true,
    keepClosingSlash: true,
    minifyJS: true,
    minifyCSS: true,
    minifyURLs: true
  };
}

let plugins = [
  new LodashPlugin(),
  new HtmlWebpackPlugin(htmlWebpackPluginConfig),
  new webpack.DefinePlugin(globals),
  new webpack.ProvidePlugin({
    // Empty
  })
];

if (env.isDev) {
  plugins.push(
    new webpack.NamedModulesPlugin(),
    new webpack.DllReferencePlugin({
      context: '.',
      manifest: require(path.resolve(paths.dllOutputPath, 'deps.json'))
    }),
    new AddAssetHtmlPlugin({
      filepath: require.resolve(`${paths.dllOutputPath}/deps.dll.js`),
      includeSourcemap: true
    })
  );
}

let rules = [{
  test: /\.js$/,
  include: paths.srcPath,
  loader: 'babel-loader',
  options: {
    cacheDirectory: env.isDev
  }
}];

// scss files in src
rules.push({
  test: /\.scss/,
  include: /src/,
  use: [{
    loader: 'style-loader'
  }, {
    loader: 'css-loader',
    options: {
      modules: true,
      sourceMap: true,
      minimize: false,
      importLoaders: 1,
      localIdentName: '[name]-[local]-[hash:base64:5]'
    }
  }, {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      includePaths: [
        path.resolve(paths.srcPath, 'styles')
      ]
    }
  }]
});

// css files in node_modules
rules.push({
  test: /\.css/,
  exclude: /src/,
  use: [{
    loader: 'style-loader'
  }, {
    loader: 'css-loader',
    options: {
      sourceMap: true
    }
  }]
});

// scss files in node_modules
rules.push({
  test: /\.scss/,
  exclude: /src/,
  use: [{
    loader: 'style-loader'
  }, {
    loader: 'css-loader',
    options: {
      sourceMap: true
    }
  }, {
    loader: 'sass-loader',
    options: {
      sourceMap: true
    }
  }]
});

rules.push({
  test: /\.svg$/,
  include: path.resolve(paths.srcPath, 'icons'),
  use: [{
    loader: 'svg-sprite-loader',
    options: {
      name: '[name].[hash]',
      prefixize: true
    }
  }]
});

let output = {
  path: paths.buildPath,
  publicPath: '/',
  filename: '[name].js',
  chunkFilename: '[name].chunk.js'
};

if (env.isProd) {
  output.filename = 'server.js';
  output.chunkFilename = 'server.chunk.js';
  // output.filename = '[name].[chunkhash].js';
  // output.chunkFilename = '[name].[chunkhash].chunk.js';
}

const devtool = env.isDev ? 'cheap-module-eval-source-map' : false;

let webpackConfig = {
  devtool,
  entry,
  output,
  plugins,
  module: {
    rules
  }
};

module.exports = webpackConfig;
