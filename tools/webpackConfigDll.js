const path = require('path');
const webpack = require('webpack');
const _ = require('lodash');

const paths = require('./paths');
const pkg = require('../package.json');
const dllConfig = pkg.dll;

let deps = Object.keys(pkg.dependencies);

if (dllConfig.exclude) {
  deps = _.remove(deps, (d) => !dllConfig.exclude.includes(d));
}

module.exports = {
  entry: {
    deps
  },
  devtool: 'source-map',
  output: {
    filename: '[name].dll.js',
    path: paths.dllOutputPath,
    library: '[name]_[hash]'
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.join(paths.dllOutputPath, '[name].json'),
      name: '[name]_[hash]'
    })
  ],
  performance: {
    hints: false
  }
};
