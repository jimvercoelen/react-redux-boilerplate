const path = require('path');
const pkg = require('../package.json');

const basePath = path.resolve(__dirname, '../');
const srcPath = path.join(basePath, 'src');
const buildPath = path.join(basePath, 'build');
const dllOutputPath = path.join(process.cwd(), `node_modules/${pkg.name}-dll`);

module.exports = {
  basePath,
  srcPath,
  buildPath,
  dllOutputPath
};
