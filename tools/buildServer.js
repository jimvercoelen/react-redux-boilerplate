const paths = require('./paths');
const browserSync = require('browser-sync');
const historyFallback = require('connect-history-api-fallback');
const compression = require('compression');

browserSync({
  open: true,
  server: {
    baseDir: paths.buildPath,
    middleware: [
      historyFallback(),
      compression()
    ]
  }
});
